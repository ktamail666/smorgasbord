#ifndef SMORGASBORD_GENERATEIMAGE_HPP
#define SMORGASBORD_GENERATEIMAGE_HPP

namespace Smorgasbord {

class Image;

void GenerateWhiteNoise(Image& image, int seed);

}

#endif // SMORGASBORD_GENERATEIMAGE_HPP
