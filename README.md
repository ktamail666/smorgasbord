# Smorgasbord rendering library (Version NaN)

Smorgasbord is a real-time rendering library aiming to aid experimentation
and helps implementing rendering pipelines in a concise way without being
vague about the underlying operations.

In its current state, I would not recommend using this library. I expect
frequent API breaking changes until it becomes more feature complete.

## Compiling the source

The library needs the "dep_src" repository contents to compile. Set the
DEP_SRC_DIRECTORY cmake option or clone the two repos next to each other.

The build scripts are tested under QtCreator/MinGW32 and VS2017/MSVC14.

## Contributions

I reserve the right to relicense the library for any purpose in its entirety,
with or without third-party contributions. Even if it means a closed source
and/or commercial license. Only contribute changes to the library if you're
OK with that.

## License

Copyright 2018 Gábor Könyvesi

See the LICENSE file for licensing terms
